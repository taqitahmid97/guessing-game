public class GuessGame {
    Player p1;
    Player p2;
    Player p3;

    public void startGame() {
        p1 = new Player();
        p2 = new Player();
        p3 = new Player();
        p1.player_name = "player one";
        p2.player_name = "player two";
        p3.player_name = "player three";

        int guessp1 = 0;
        int guessp2 = 0;
        int guessp3 = 0;
       

        int targetNumber = (int) (Math.random() * 10);
        System.out.println("I'm thinking of a number between 0 and 9...");
        while (true) {
            System.out.println("Number to guess is  " + targetNumber);
            int flag=0;
            p1.guess();
            p2.guess();
            p3.guess();
            guessp1 = p1.number;
            
            p1.print();

            guessp2 = p2.number;
            p2.print();
            guessp3 = p3.number;
            p3.print();
            if (guessp1 == targetNumber) {
               
                System.out.println("Player one got it right.");
                System.out.println("Game Over.");
                flag=1;
                break;
            }
           if (guessp2 == targetNumber) {
               
                System.out.println("Player two got it right.");
                System.out.println("Game Over.");
                flag=1;
                break;
            }
            if (guessp3 == targetNumber) {
                
                System.out.println("Player three got it right.");
                System.out.println("Game Over.");
                flag=1;
                break;
            }
          
            if(flag==0)
            {
                System.out.println("Players will have to try again."); 
            }
        }

    }

}
